import pyaudio
import wave

DEFAULT_CHUNK = 8192 
DEFAULT_FORMAT = pyaudio.paInt16 #paInt8
DEFAULT_CHANNELS = 1
DEFAULT_RATE = 48000 #sample rate
DEFAULT_RECORD_SECONDS = 10
DEFAULT_WAVE_OUTPUT_FILENAME = "test"
DEFAULT_OUTPUT_FOLDER = "dataset"

def record_new_track(r_chunk = DEFAULT_CHUNK,
			 		 r_format = DEFAULT_FORMAT,
			 		 r_channels = DEFAULT_CHANNELS,
			 		 r_rate = DEFAULT_RATE,
			 		 r_secs = DEFAULT_RECORD_SECONDS,
			 		 r_filename = DEFAULT_WAVE_OUTPUT_FILENAME,
			 		 r_outputfolder = DEFAULT_OUTPUT_FOLDER):

	p = pyaudio.PyAudio()
	stream = p.open(format=r_format,
	                channels=r_channels,
	                rate=r_rate,
	                input=True,
	                frames_per_buffer=r_chunk) #buffer

	frames = []

	for i in range(0, int(r_rate / r_chunk * r_secs)):
	    data = stream.read(r_chunk)
	    frames.append(data) # 2 bytes(16 bits) per channel

	stream.stop_stream()
	stream.close()
	p.terminate()

	wf = wave.open(r_outputfolder+ "/" +r_filename + ".wav", 'wb')
	wf.setnchannels(r_channels)
	wf.setsampwidth(p.get_sample_size(r_format))
	wf.setframerate(r_rate)
	wf.writeframes(b''.join(frames))
	wf.close()