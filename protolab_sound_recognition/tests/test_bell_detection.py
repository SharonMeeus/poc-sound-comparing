__author__ = 'lgeorge'

import os
import glob
import subprocess

from sound_classification import classification_service
from test_common import _get_training_data, wget_file
import pytest
import record
import numpy as np 

def test_classifier_simple():
    """
    just check that the service is correctly installed
    :return:
    """
    sound_classification_obj = classification_service.SoundClassification()
    assert(True)


@pytest.mark.parametrize("enable_calibration_of_score", [(False), (True)])
def test_bell_detection(enable_calibration_of_score, arg_testfile):
    dataset_path = 'dataset/'
    file_regexp = os.path.join(dataset_path, '*.wav')
    files = glob.glob(file_regexp)
    sound_classification_obj = classification_service.SoundClassification(wav_file_list=files, calibrate_score=True)
    sound_classification_obj.learn()
    wav_file_path = "test_data/%s.wav" % (arg_testfile)
    res = sound_classification_obj.processed_wav(wav_file_path)
    print "** OUTPUT TEST **"
    score = 0
    class_predicted = ""
    for x in res:
        # print (x.timestamp_start, x.timestamp_end, x.class_predicted, 'confidence= \n', x.confidence, 'score= \n', x.score) 
        if score < x.score:
            score = x.score
            class_predicted = x.class_predicted
        
    print ("We think that this is:" + class_predicted)


def record_test_from_mic_and_recognize(arg_secs=20, arg_filename="test"):
    secs = arg_secs
    filename = arg_filename
    output_folder = "test_data"
    print "start recording"
    record.record_new_track(r_secs=secs, r_filename=filename, r_outputfolder = output_folder)
    print "done recording"
    print "recognizing..."
    test_bell_detection(True, filename)

def record_new_dataset_from_mic(arg_name="test"):
    filename = arg_name
    output_folder = "dataset"
    print "start recording"
    for x in range(1,13):
        rnd_secs = np.random.randint(2,6)
        full_filename = filename + "-" + str(x) + "-Me"
        record.record_new_track(r_secs=rnd_secs, r_filename=full_filename, r_outputfolder = output_folder)
    print "done recording"

