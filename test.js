(function() {
	var express = require('express');
	var app = express();
	var http = require('http').Server(app);
	var io = require('socket.io')(http);
	var ffmpeg = require('ffmpeg');
	var fpcalc = require('fpcalc');

	app.get('/', function(req, res){
		res.sendFile(__dirname + '/index.html');
	});

	app.use(express.static(__dirname + '/public'));

	io.on('connection', function(socket){
		console.log('connected');
		var fp_o = getFingerprint("mp3/test.mp3");
	});

	http.listen(3000, function(){
	  console.log('listening on *:3000');
	});

	function getFingerprint(file_path) {
		fpcalc(file_path, function(err, result) {
		  if (err) throw err;
		  fp_o = {file: result.file, duration: result.duration, fingerprint: result.fingerprint}
		  io.emit("fingerprint", fp_o);
		});
	}
})();