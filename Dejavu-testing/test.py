import warnings
import json
warnings.filterwarnings("ignore")

from dejavu import Dejavu
from dejavu.recognize import FileRecognizer, MicrophoneRecognizer
import dejavu.record as record

# load config from a JSON file (or anything outputting a python dictionary)
with open("dejavu.cnf.SAMPLE") as f:
    config = json.load(f)

	# create a Dejavu instance
djv = Dejavu(config)

def fingerprint_from_file(arg_a):
	filename = arg_a
	djv.fingerprint_file(filepath="mp3/"+filename)

def recognize_from_file(arg_a):
	filename = arg_a
	print "Recognizing from " + filename + "..."
	song = djv.recognize(FileRecognizer, "testmp3/"+filename)
	print "From file we recognized: %s\n" % song


def fingerprint_from_mic(arg_a, arg_b):
	secs = arg_a
	filename = arg_b
	record.record_new_track(r_secs=secs, r_filename=filename)
	djv.fingerprint_file(filepath="mp3/"+filename)

def recognize_from_mic(arg_a):

	secs = arg_a
	song = djv.recognize(MicrophoneRecognizer, seconds=secs)
	if song is None:
		print "Nothing recognized -- did you play the song out loud so your mic could hear it? :)"
	else:
		print "From mic with %d seconds we recognized: %s\n" % (secs, song)



