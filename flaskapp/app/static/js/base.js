$(document).ready(function(){
    $('ul.tabs').tabs();

    $("#mic button").click(function () {
      $(this).addClass("disabled")
      $(".mic_status").removeClass("hide");
      $(".mic_status span.status").length ? $(".mic_status span.status").text("Recording...") : $(".mic_status").append("<span class='status'>Recording...</span>");
      record_and_recognize();
    });

    $("#dataset button").click(function () {
      $(this).addClass("disabled")
      if ($("#dataset input").val().trim() !== "") {
        mydata = $("#dataset input").val().trim();
        $(".dataset_status").removeClass("hide");
        $(".dataset_status span.status").length ? $(".dataset_status span.status").text("Recording...") : $(".dataset_status").append("<span class='status'>Recording...</span>"); 

        add_to_dataset($("#dataset input").val().trim()); 
      } else { 
        alert("Please enter a name");
      }
    });

    function record_and_recognize() {
      r_ajax({url: 'record', success_function: myfunc});
      function myfunc (response) {
        $(".mic_status .status").empty().append("Done recording. Recognizing...");
        recognize(response);
      }
    }

    function recognize(filename) {
      r_ajax({url: "recognize", data: filename, success_function: myfunc});
      function myfunc (response) {
        var message = "";
        responsedata = $.parseJSON(response)

        if(responsedata.certain) {
          message = "We think that this is: " + responsedata.sound;
        } else {
          message = "We don't really know. Our best guess is: " + responsedata.sound;
        }
        $("#mic button").removeClass("disabled");
        alert(message);
        $(".mic_status .status").empty().append(message);   
      }
    }

    function add_to_dataset(filename) {
      r_ajax({url: "add_to_dataset", data: filename, success_function: myfunc});

      function myfunc (response) {
        alert("Saved to dataset!");
        $(".dataset_status .status").empty().append("Saved to dataset as " + response);   
        $("#dataset button").removeClass("disabled");
      }
    }



    // o_args = object with url and function, optional data
    function r_ajax (o_args) {

      var mydata = o_args.data ? o_args.data : "";

      $.ajax({
            type: "POST",
            url: '/' + o_args.url,
            contentType: 'application/json',
            data: mydata,
            success: function(response){
                o_args.success_function(response)   
            },

      });
    }


  });