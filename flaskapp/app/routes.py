from flask import Flask, render_template, request
import recognizer.tests.soundrecognizer as soundrecognizer
 
app = Flask(__name__)      
 
@app.route('/')
def show_app():
  return render_template('app.html')

@app.route('/record', methods=['POST'])
def record():
	sound = soundrecognizer.record_test_from_mic()
	return sound

@app.route('/recognize', methods=['POST'])
def recognize():
	filename = request.get_data()
	sound = soundrecognizer.recognize_sound(True, filename)
	return sound.replace("_", " ");

@app.route('/add_to_dataset', methods=['POST'])
def add():
	filename = request.get_data()
	filename = filename.replace(" ", "_")
	sound = soundrecognizer.record_new_dataset_from_mic(filename)
	return filename


 
if __name__ == '__main__':
  app.run(debug=True)
