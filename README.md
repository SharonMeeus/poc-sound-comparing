# Sound Comparing #

- Ultimate goal is to create an application that can recognize someone's voice.
- At least trying to recognize sounds (p.e. clapping, whistling, laughing,...)

# Research # 

- Need to implement audio fingerprinting. --> Difficulty: Fingerprinting needs to be forgiving.
- Find similarity between two fingerprints.

# How to run test from fpcalc #

- Make sure fpcalc is installed: https://github.com/parshap/node-fpcalc or https://acoustid.org/chromaprint (Don't forget to set your path if necessary)
- Make sure node is installed
- run "npm install" in terminal to install dependencies
- run app using "node ." or "node test.js"
- go to http://localhost:3000/ to see the output

# How to run test from dejavu #

- Install Python and other packacges: https://github.com/worldveil/dejavu/blob/master/INSTALLATION.md
- Change the settings in dejavu.cnf.SAMPLE to match your mysql database

- To record a new wav file and fingerprint it run: python -c 'import test; test.fingerprint_from_mic(secs, filename.wav);'
  For example: python -c 'import test; test.fingerprint_from_mic(5, "clapping.wav");' File will be stored in the folder mp3 (create if necessary)

- To recognize from mic: python -c 'import test; test.recognize_from_mic(secs);'

- To fingerprint from file, you'll have to store the file you want to fingerprint in the folder 'mp3' and then run: python -c 'import test; test.fingerprint_from_file(filename);'
  For example: python -c 'import test; test.fingerprint_from_file("Test.mp3");'

 - To recognize from file, you'll have to store a file in folder 'testmp3' (create if necessary) and then run: python -c 'import test; test.recognize_from_file(filename);'

# Update Aug 1 #

- Did tests with fpcalc from AcoustID. Posted a fingerprint (with track and artist_name included), but on get request the metadata doesn't return. This means it's impossible to know what song or sound you're comparing with. (It seems you can only get metadata back from MusicBrainz itself...)

# Update Aug 2 #

- Found Audio fingerprinting and recognition algorithm implemented in Python called "Dejavu": https://github.com/worldveil/dejavu. Will need to learn a bit of Python. Going to try to combine this with Flask. Have tested the example from dejavu. So far so good. TBC.

#Update Aug 4

- Testing with Dejavu. Made file called "test.py" to record, save and fingerprint a new .wav file. Also possible to recogize input, but commented that out.
  -> Did some tests with own voice, but gave a real small amount of confidence. Adding more voices will be difficult.

#Update Aug 5
- Testing with voices or sounds don't seem to work. Getting very low confidence and wrong matching. Searching for solution...
